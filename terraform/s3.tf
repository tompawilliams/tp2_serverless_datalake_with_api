g# TODO : Create a s3 bucket with aws_s3_bucket
resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-bucket-Williams-Wecker-Tranchet"
  acl    = "private"
  force_destroy = true

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object
resource "aws_s3_bucket_object" "object" {
  bucket = "s3-job-offer-bucket-Williams-Wecker-Tranchet"
  key    = "job-offer/raw"
  source = "/dev/null"


}
# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_sns_topic" "topic" {
  name = "s3-event-notification-topic"

  policy = <<POLICY
{
    "Version":"2012-10-17",
    "Statement":[{
        "Effect": "Allow",
        "Principal": {"AWS":"*"},
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:*:*:s3-event-notification-topic",
        "Condition":{
            "ArnLike":{"aws:SourceArn":"${aws_s3_bucket.bucket.arn}"}
        }
    }]
}
POLICY
}

resource "aws_s3_bucket" "bucket" {
  bucket = "s3-job-offer-bucket-Williams-Wecker-Tranchet"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket.id

  topic {
    topic_arn     = aws_sns_topic.topic.arn
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".log"
  }
}